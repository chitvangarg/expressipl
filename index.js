const matchPlayedPerYear = require("./src/server/1-matches-per-year.cjs")
const matchesWonPerTeamPerYear = require("./src/server/2-matches-won-per-team-per-year.cjs")
const extraRunsConceededPerTeam = require("./src/server/3-extra_runs_conceeded_in_2016.cjs")
const economicalBowlerIn2015 = require("./src/server/4-10-economical-bowler.cjs")
const tossWonMatchWon = require("./src/server/5-toss-won-match-won.cjs")
const highestPlayerOfTheMatchPerSeason = require("./src/server/6-highest-award-each-season.cjs")
const strikerateForEachBatsmen = require("./src/server/7-strike-rate-for-batsmen-for-each-season.cjs")
const maxDismissalOfPlayer = require("./src/server/8-dissmissals-count.cjs")
const bestEconomyPlayerInSuperOver = require("./src/server/9-best-economy-super-over.cjs")

// config file
const { port } = require("./config")

// Server creation
const express = require("express")
const app = express()

app.use(express.json())

app.listen(port, () => {
    console.log(`Server listening successfully: ${port}`)
})

//1

app.get("/matchPlayedPerYear", async (req, res) => {
    matchPlayedPerYear()
        .then((data) => {
            res.json(data)
            return
        })
        .catch((err) => {
            console.log(err.message)
        })
})

//2
app.get("/matchesWonPerTeamPerYear", (req, res) => {
    matchesWonPerTeamPerYear()
        .then((data) => {
            res.json(data)
            return
        })
        .catch((err) => {
            console.log(err.message)
        })
})

// 3

app.get("/extraRunsConceededPerTeam", (req, res) => {
    extraRunsConceededPerTeam()
        .then((data) => {
            res.json(data)
            return
        })
        .catch((err) => {
            console.log(err.message)
        })
})

// 4

app.get("/economicalBowlerIn2015", (req, res) => {
    economicalBowlerIn2015()
        .then((data) => {
            res.json(data)
            return
        })
        .catch((err) => {
            console.log(err.message)
        })
})

// 5

app.get("/tossWonMatchWonRes", (req, res) => {
    tossWonMatchWon()
        .then((data) => {
            res.json(data)
            return
        })
        .catch((err) => {
            console.log(err.message)
        })
})

// 6
app.get("/highestPlayerOfTheMatchPerSeasonRes", (req, res) => {
    highestPlayerOfTheMatchPerSeason()
        .then((data) => {
            res.json(data)
            return
        })
        .catch((err) => {
            console.log(err.message)
        })
})

// 7
app.get("/strikerateForEachBatsmen", (req, res) => {
    strikerateForEachBatsmen()
        .then((data) => {
            res.json(data)
            return
        })
        .catch((err) => {
            console.log(err.message)
        })
})

// 8
app.get("/maxDismissalOfPlayer", (req, res) => {
    maxDismissalOfPlayer()
        .then((data) => {
            res.json(data)
            return
        })
        .catch((err) => {
            console.log(err.message)
        })
})

// 9
app.get("/bestEconomyPlayerInSuperOver", (req, res) => {
    bestEconomyPlayerInSuperOver()
        .then((data) => {
            res.json(data)
            return
        })
        .catch((err) => {
            console.log(err.message)
        })
})
