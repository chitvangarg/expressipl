const csvtoJson = require("csvtojson")
const path = require("path")

async function bestEconomyPlayerInSuperOver() {
    const deliveries = await csvtoJson().fromFile(
        path.resolve(__dirname, "../data/deliveries.csv")
    )

    const superOverBowlers = deliveries.filter((delivery) => {
        return delivery.is_super_over == 1
    })

    // producting total_runs, balls delivered and economy of the particular bowler
    const result = superOverBowlers.reduce((acc, event) => {
        if (!acc[event.bowler]) {
            acc[event.bowler] = {
                runs: Number(event.total_runs),
                balls: 1,
                eco: 0,
            }
        } else {
            acc[event.bowler].runs += Number(event.total_runs)
            acc[event.bowler].balls += 1
            let economy = (acc[event.bowler].runs / acc[event.bowler].balls) * 6
            acc[event.bowler].eco = economy
        }
        return acc
    }, {})

    // Storing object enteried in an array
    let enteries = Object.entries(result)
    let superOverEconomyArr = enteries.sort((prev, curr) => {
        if (prev[1].eco > curr[1].eco) {
            return 1
        } else if (prev[1].eco < curr[1].eco) {
            return -1
        } else {
            return 0
        }
    })

    // extracting top bowler based on econmy in super over
    let bestBowler = superOverEconomyArr.slice(0, 1)

    return bestBowler
}

module.exports = bestEconomyPlayerInSuperOver
