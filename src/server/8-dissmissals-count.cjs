const csvtoJson = require("csvtojson")
const path = require("path")

async function dissmissalCount() {
    const deliveries = await csvtoJson().fromFile(
        path.resolve(__dirname, "../data/deliveries.csv")
    )

    // Creating an Object for containing information about dissmissals
    const p1_dismiss_p2_max = deliveries.reduce((acc, delivery) => {
        const player1 = delivery.bowler
        const player2 = delivery.player_dismissed
        const key = String(player1 + " dismissed " + player2)

        if (
            player2 != undefined &&
            player2 != null &&
            player2.trim().length > 0
        ) {
            if (!acc[key]) {
                acc[key] = 1
            } else {
                acc[key]++
            }
        }
        return acc
    }, {})

    // Sorting obtained result and return top value for the best one

    let enteries = Object.entries(p1_dismiss_p2_max)

    enteries.sort((a, b) => b[1] - a[1])

    const res = {}

    res["all_dismissal"] = p1_dismiss_p2_max
    res["max_dissmissal"] = `${enteries[0][0]} ${enteries[0][1]} times`

    return res
}

module.exports = dissmissalCount
