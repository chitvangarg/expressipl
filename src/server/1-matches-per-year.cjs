const csvtoJson = require("csvtojson")
const path = require("path")

async function matchPlayedPerYear() {
    const matches = await csvtoJson().fromFile(
        path.resolve(__dirname, "../data/matches.csv")
    )

    let years = matches.map((elem) => elem.season)

    // if resulting object has year value then add else assign it with 0
    let countPerSeason = years.reduce((accum, season) => {
        accum[season] = (accum[season] || 0) + 1
        return accum
    }, {})

    return countPerSeason
}

module.exports = matchPlayedPerYear
