const csvtoJson = require("csvtojson")
const path = require("path")

async function extraRunsConceededPerTeam() {
    const matches = await csvtoJson().fromFile(
        path.resolve(__dirname, "../data/matches.csv")
    )
    const deliveries = await csvtoJson().fromFile(
        path.resolve(__dirname, "../data/deliveries.csv")
    )

    const years = matches.filter((match) => {
        return match.season === "2016"
    })

    const matchId = years.map((match) => {
        return match.id
    })

    // Filtering Ids that are available in matches
    const filteredDeliveries = deliveries.filter((delivery) =>
        matchId.includes(delivery.match_id)
    )

    // Combining extra runs for a particular team
    const extraRuns = filteredDeliveries.reduce((totalExtra, elem) => {
        totalExtra[elem.bowling_team] =
            (totalExtra[elem.bowling_team] ?? 0) + parseInt(elem.extra_runs)
        return totalExtra
    }, {})

    return extraRuns
}

module.exports = extraRunsConceededPerTeam
