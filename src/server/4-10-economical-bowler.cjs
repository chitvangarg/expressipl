const csvtoJson = require("csvtojson")
const path = require("path")

async function economicalBowlerIn2015() {
    const matches = await csvtoJson().fromFile(
        path.resolve(__dirname, "../data/matches.csv")
    )
    const deliveries = await csvtoJson().fromFile(
        path.resolve(__dirname, "../data/deliveries.csv")
    )

    const years = matches.filter((match) => match.season === "2015")

    const matchId = years.map((match) => match.id)

    // filter deliveries in 2015
    const deliveriesIn2015 = deliveries.filter((delivery) =>
        matchId.includes(delivery.match_id)
    )

    // Gathering all bowlers runs, their deliveries bowled and calculated strike rate in 2015
    const result = deliveriesIn2015.reduce((acc, elem) => {
        const bowler = elem.bowler

        if (acc[bowler] === undefined) {
            acc[bowler] = {
                runs: Number(elem.total_runs),
                balls: 1,
                economy: undefined,
            }
        } else {
            acc[bowler].runs += Number(elem.total_runs)
            acc[bowler].balls++
            acc[bowler].economy = (acc[bowler].runs / acc[bowler].balls) * 6
        }

        return acc
    }, {})

    // Sorting result in ascending order and then extracting top 10 values
    let enteries = Object.entries(result)

    let top10Bowlers = enteries
        .sort((prev, curr) => {
            return prev[1].economy - curr[1].economy
        })
        .slice(0, 10)

    let ans = top10Bowlers.reduce((accum, elem) => {
        accum[elem[0]] = elem[1]
        return accum
    }, {})

    return ans
}

module.exports = economicalBowlerIn2015
