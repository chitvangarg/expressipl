const csvtoJson = require("csvtojson")
const path = require("path")

async function highestPlayerOfTheMatchPerSeason() {
    const matches = await csvtoJson().fromFile(
        path.resolve(__dirname, "../data/matches.csv")
    )

    // Creating Object player of the match per person per year
    const playerOfTheMatchPerSeason = matches.reduce(
        (playerOfTheMatchPerSeason, elem) => {
            if (playerOfTheMatchPerSeason[elem.season] === undefined) {
                const player = {}

                player[elem.player_of_match] = 1
                playerOfTheMatchPerSeason[elem.season] = player
            } else {
                playerOfTheMatchPerSeason[elem.season][elem.player_of_match] =
                    (playerOfTheMatchPerSeason[elem.season][
                        elem.player_of_match
                    ] ?? 0) + 1
            }

            return playerOfTheMatchPerSeason
        },
        {}
    )

    // Sorting Extracted playerOfTheMatch to get highest award winnig player per year

    let entriesKeysArray = Object.keys(playerOfTheMatchPerSeason)

    let res = entriesKeysArray.reduce((accum, seasonKey) => {
        let seasonEntry = Object.entries(playerOfTheMatchPerSeason[seasonKey])

        seasonEntry.sort((first, second) => second[1] - first[1])

        const value = {}

        value[seasonEntry[0][0]] = seasonEntry[0][1]

        accum[seasonKey] = value

        return accum
    }, {})

    return res
}

module.exports = highestPlayerOfTheMatchPerSeason
