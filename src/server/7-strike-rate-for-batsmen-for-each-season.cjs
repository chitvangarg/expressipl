const csvtoJson = require("csvtojson")
const path = require("path")

async function strikeRatePerYearPerPerson() {
    const matches = await csvtoJson().fromFile(
        path.resolve(__dirname, "../data/matches.csv")
    )
    const deliveries = await csvtoJson().fromFile(
        path.resolve(__dirname, "../data/deliveries.csv")
    )
    // Extracting id and season in new object
    const match_ids = matches.map((match) => {
        return { id: match.id, season: match.season }
    })

    // mereging required id season with deliveries
    const mergedDataset = deliveries.map((delivery) => {
        const year = match_ids.find((match) => {
            return match.id === String(delivery.match_id)
        })

        return { ...delivery, Year: year.season }
    })

    // Using reduce to find strike rate of batsman per season
    const batsmen_data = mergedDataset.reduce((acc, deliveries) => {
        const batsman = deliveries.batsman
        const runs = Number(deliveries.batsman_runs)
        const season = deliveries.Year

        if (!acc[batsman]) {
            acc[batsman] = {}
        }
        if (!acc[batsman][season]) {
            acc[batsman][season] = {
                runs_scored: runs,
                balls_faced: 1,
                strike_rate: undefined,
            }
        } else {
            if (!acc[batsman][season].strike_rate) {
                acc[batsman][season].strike_rate =
                    (acc[batsman][season].runs_scored /
                        acc[batsman][season].balls_faced) *
                    100
            }
            acc[batsman][season].runs_scored += runs
            acc[batsman][season].balls_faced++
            acc[batsman][season].strike_rate =
                (acc[batsman][season].runs_scored /
                    acc[batsman][season].balls_faced) *
                100
        }

        return acc
    }, {})

    return batsmen_data
}

module.exports = strikeRatePerYearPerPerson
