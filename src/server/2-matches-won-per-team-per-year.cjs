const csvtoJson = require("csvtojson")
const path = require("path")

async function matchesWonPerTeamPerYear() {
    const matches = await csvtoJson().fromFile(
        path.resolve(__dirname, "../data/matches.csv")
    )

    let matchWonPerTeamPerSeason = matches.reduce(
        (matchWonPerTeam, element) => {
            const { winner, season } = element

            // If season is not in the resulting then add it
            if (matchWonPerTeam[winner] == undefined) {
                matchWonPerTeam[winner] = {}
            }

            // if particular team does not belong to winner of that season (won first time)
            if (matchWonPerTeam[winner][season] === undefined) {
                matchWonPerTeam[winner][season] = 1
            } else {
                matchWonPerTeam[winner][season]++
            }

            return matchWonPerTeam
        },
        {}
    )

    return matchWonPerTeamPerSeason
}

module.exports = matchesWonPerTeamPerYear
