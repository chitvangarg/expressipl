const csvtoJson = require("csvtojson")
const path = require("path")

async function tossWonMatchWon() {
    const matches = await csvtoJson().fromFile(
        path.resolve(__dirname, "../data/matches.csv")
    )

    // Extracting team and toss winner team for matches data
    let match = matches.map((match) => {
        return {
            team: match["winner"],
            toss: match["toss_winner"],
        }
    })

    // increment tha value of particular team if both toss and winner are same
    let result = match.reduce((accum, element) => {
        if (element.team === element.toss) {
            accum[element.team] = (accum[element.team] ?? 0) + 1
        }

        return accum
    }, {})

    return result
}

module.exports = tossWonMatchWon
